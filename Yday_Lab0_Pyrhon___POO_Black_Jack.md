# Yday Lab0 Pyrhon : POO Black Jack


## Intro

Dans ce tp vous aurez 3 parties à réaliser. Ces parties sont complémentaires, et sont en réalité la séparation d'un projet en 3 étapes.

Chaque partie vaut un certain nombre de points qui vous est indiqué à chaque fois. Mais pour vous donner une idée globale, dans la notation il y est compris la clarté du code ainsi que la fonctionnalité de ce dernier (faites donc attention au nom de vos variables/fonctions).

Comme il est question de point, vous vous doutez que le TP sera à rendre et contribuera à la note du ydays. Sur ces mots, bonne chance à vous :)

## Sommaire
- [POO](#poo)
  - [Intro](#intro)  
  - [Sommaire](#sommaire)
  - [I. Card](#i-card-3-points)
  - [II. Cards](#ii-cards-5-points)
  - [III. Game](#iii-game-7-points)



## I. Card

![](https://i.imgur.com/Cvqijr5.png)


Cette partie consiste à créer une classe`Card` qui sera à la base de notre jeu.


Cette classe possède 2 attributs :
    - `value` qui permet de stocker la valeur de la carte,
    - `color` qui permet de stocker le signe de la carte.
et 3 méthodes : 
    - 2 getters `getValue` et `getColor` pour permettre l'accés aux valeurs,
    - une méthode `displayCard` qui permet d'afficher le contenu des attributs de la classe.
    
    
## II. Cards

![](https://i.imgur.com/ovPwYy4.png)

Cette partie a pour but de créer une classe`Cards` qui se compose en un ensemble d'objets `Card`.


Cette classe possède 2 attributs :
    - `listcard` qui permet de stocker la liste de `Card`,
    - `countcard` qui permet de stocker le nombre de carte dans la liste.
    
Cette classe `cards` possède 2 méthodes : 
    - un getter `getCount` pour permettre l'accés à la valeur,
    -une méthode `draw` qui simule une pioche: on récupère la valeur la première carte du paquet, on supprime la carte      du "paquet" et enfin on renvoie la carte piochée.
    
    
## III. Game

Pour réaliser cette partie, j'ai choisi de créer une nouvelle classe que j'ai appelé `Player` ainsi que 2 fonctions `createDeck` et `game`.

![](https://i.imgur.com/Hu5kalq.png)

Le but de la classe `Player` est de faciliter la gestion des données relatives aux cartes des joueurs ainsi que la valeur de leur main.

Cette classe possède 2 attributs : 
    - `hand` qui est une liste de `Card` permettant de stocker la main du joueur,
    - `total` qui permt de stocker la valeur de totale de la main du joueur.
    
Cette classe possède 2 méthodes : 
    - un getter `getTotal` qui permet l'accés à la valeur de la main du joueur,
    - une méthode `addToTotal` qui permet d'ajouter la valeur d'une carte à celle de la main du joueur, on se sert d'un tuple pour faire correspondre la valeur des têtes dans la liste à celle du jeu, pour le cas de l'as on commence par lui attribuer la valeur 11, si celà fait perdre le joueur alors on a lui attribue la valeur1 1.

    
Pour mettre en place la partie, j'ai créé 2 fonctions : 

![](https://i.imgur.com/upYv54K.png)


La fonction `createDeck` permet de générer un deck neuf. Pour celà par déclarer un tuple `colors` qui permettra d'attribuer les couleurs des cartes, ensujite on se sert d'une double boucle for(une pour la valeur numérique de la carte et l'autre pour la couleur) pour générer l'ensemvle des `Card`, enfin on construit un objet `Cards` auquel on lie notre liste, on mélangeles cartes à l'aide de la foinction `shuffle` du package `random` et on renvoie le deck fraichement créé.


La fonction `game` est celle qui définit le déroulement de la partie :

![](https://i.imgur.com/3SZdj4g.png)

On commence par créer 2 objets `Player`, un pour le joueur et un pour le croupier, ensuite on distribue les cartes à l'aide d'une boucle for et on vérifie si le jouer n'a pas pioché un blackjack.

Une fois celà fait le tout du joueur du joueur peut commencer : 

![](https://i.imgur.com/Xw9P2xa.png)

Tant que le score total de la main est inférieur à 22, c'est à dire que le joueur n'a pas perdu, on lui propose de piocher une nouvelle carte, s'il acquiesce on va alors piocher une nouvelle carte la rajouter à sa main et on vérifie son nouveau score, si le joueur ne veut plus de carte suppémentaire, on quitte la boucle et on commence le tour du croupier : 

![](https://i.imgur.com/7P0wlEU.png)

Pour respecter les règles du blackjack, on fait piocher le croupier tant que la valeur de sa main ne dépasse pas 16.

Enfin, on compare les scores et on déclare le vainqueur.